import gulp from 'gulp';
var closureCompiler = require('google-closure-compiler').gulp();

gulp.task('build:closure', () => {
	return gulp.src(pkg.name + '.closure.js')
		.pipe(closureCompiler({
			checks_only: 'true',
			externs: 'node_externs.js',
			language_in: 'ECMASCRIPT6_STRICT',
			warning_level: 'VERBOSE'
		}));
});